# GobbleHook
This repository is a WIP on a hook to add custom logic to Pac-Man Championship Edition 2. It makes use of the excellent
[Harmony library](https://github.com/pardeike/Harmony) to patch up game logic at runtime.

Since Pac-Man CE 2 (PMCE2) is a totally unprotected Unity game, the game's code is available as a managed Mono assembly
in the `Assembly-CSharp.dll` file. The library released in this repository (in the following called "hook assembly") can
dispatch original logic to custom one implemented in a third library compiled by you.

## Setting up the game
Prepare the game as follows to make it interact with the hook assembly.
- Locate `Assembly-CSharp.dll` in the game data folder.
- Place the hook library and `0Harmony.dll` in the same folder.
- Manually patch `Assembly-CSharp.dll` to make the static constructor of `global::Framework` class call
  s`Syroot.GobbleHook.Core.Loader.Initialize()`.

## Adding custom code
Provide a third assembly compiled by you, which contains the custom code to run.
- Create a new .NET 4.0 class library project in Visual Studio
  ([Community 2017](https://www.visualstudio.com/downloads/) recommended).
- Add a reference to the hook, the original `Assembly-CSharp.dll`, and any further assemblies of interest.
- Set the output path to the same folder as the hook assembly.
  - The hook assembly will automatically detect your library there.
- Analyze the original game code with a .NET decompiler and determine a class and method you want to manipulate.
  
In the following, we are interested in hooking `GameFramework.OnMoveMapComplete()`.
- Create a class inheriting from `Syroot.GobbleHook.GameFrameworkHook`.
- Add an override for `OnMoveMapComplete`.
  - Call `base` in the override to execute original game logic in its place.
  - You may not want to call `base` to never execute original logic.
- Access the original `GameFramework` instance with the `Source` property.
- Access any member (e.g. private / protected / internal...) of the original instance with the `SourceMember`
  property (s. below).

Note that currently only a few hook classes and methods are provided by the hook assembly.

## Accessing hidden members
Every hook class has a `SourceMember` property, which is of type `MemberAccessor`. A `MemberAccessor` allows
simple access to hidden members of the `Source` instance.

For this, it has an indexer operator to which the name of the member is passed (and any possible parameters) as follows:

| Member Type               | Sample Call                                          |
|---------------------------|------------------------------------------------------|
| Fields / Properties (get) | `int x = (int)SourceMember["_someField"]`            |
| Fields / Properties (set) | `SourceMember["SomeProperty"] = 3`                   |
| Constructors              | `var instance = SourceMember[null]`                  |
| Constructors (params)     | `var instance = SourceMember[null, "blabla", 12]`    |
| Methods (void)            | `_ = SourceMember["HiddenMethod"]`                   |
| Methods (result)          | `int x = (int)SourceMember["CalculateNumber"]`       |
| Methods (result, params)  | `int x = (int)SourceMember["CalculateNumber", 3, 2]` |

It currently does not support attaching / removing events, destructors, or generics.

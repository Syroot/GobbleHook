﻿using System;
using PCE2;
using Syroot.GobbleHook.Core;

namespace Syroot.GobbleHook.Playground
{
    public class MyPacmanStateController : PacmanStateControllerHook
    {
        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        protected MyPacmanStateController(PacmanStateController source, Type initState) : base(source, initState) { }

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        public override void SetNextState(PacmanState state)
        {
            // Using a bomb triggers a powerup.
            //if (state == PacmanState.kBombJump)
                //GameFramework.Instance.Notify(GameFramework.NotifyLabel.POWERUP_START);
            base.SetNextState(state);
        }
    }
}

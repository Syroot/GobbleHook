﻿namespace Syroot.GobbleHook.Playground
{
    public class MyGameFramework : GameFrameworkHook
    {
        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        protected MyGameFramework(GameFramework source, ModeGame mode) : base(source, mode) { }

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        public override void OnReadyGo()
        {
            base.OnReadyGo();

            // Immediately start a powerup.
            //Source.Notify(GameFramework.NotifyLabel.POWERUP_START);
        }

        public override void OnMoveMapComplete()
        {
            base.OnMoveMapComplete();

            // Immediately start a powerup.
            //Source.Notify(GameFramework.NotifyLabel.POWERUP_START);
        }
    }
}

﻿using MapParam;
using UnityEngine;

namespace Syroot.GobbleHook.Playground
{
    public class MyPacmanCharacterHook : PacmanCharacterHook
    {
        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        protected MyPacmanCharacterHook(PacmanCharacter source, int id) : base(source, id) { }

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        public override void CheckEatEsa()
        {
            MapObject currentMapObj = Source.GetCurrentMapObj();
            if (currentMapObj == null)
                return;

            for (int i = -1; i < 4; i++)
            {
                Vector3 pos = Source.Pos + Direction.GetVectorDirection((Direction.DIR)i);
                if (Mathf.Abs(Mathf.Floor(pos.x + 0.5f) - Source.Pos.x)
                    + Mathf.Abs(Mathf.Floor(pos.y + 0.5f) - Source.Pos.y) < 1f/*increase sensitivity, normally 0.66f*/)
                {
                    MapBlockParam blockParam = currentMapObj.GetBlockParam(pos);
                    EatEsaKind eatEsaKind = blockParam.CheckEatEsa(EatEsaKind.kAll, false);
                    switch (eatEsaKind)
                    {
                        case EatEsaKind.kSpPow:
                            Source.EatSpPowerCookie(blockParam._pos);
                            break;
                        case EatEsaKind.kFruit:
                            Source.EatFruits(blockParam._pos, false);
                            break;
                        case EatEsaKind.kPow:
                            Source.EatPowerCookie(blockParam._pos);
                            break;
                        case EatEsaKind.kCookie:
                            Source.EatCookie(blockParam._pos);
                            break;
                    }
                    if (eatEsaKind != EatEsaKind.kNothing && (bool)SourceMember["_enableSoundEffectEatAfterResurrect"])
                        SourceMember["_enableSoundEffectEatAfterResurrect"] = false;
                }
            }
        }
    }
}

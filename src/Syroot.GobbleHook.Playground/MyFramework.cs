﻿using System;
using Syroot.GobbleHook.UI;
using UnityEngine;

namespace Syroot.GobbleHook.Playground
{
    public class MyFramework : FrameworkHook
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private GameTuningParam _tuning;

        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        protected MyFramework(Framework source) : base(source) { }

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        public override void Awake()
        {
            base.Awake();
        }

        public override void Update()
        {
            // Ignore fullscreen forcing.
            if (Source.WindowFocus && !Screen.fullScreen)
                Screen.SetResolution(1920, 1080, true);

            // Remaining original code following.
            Singleton<GlobalTimer>.Instance.Update();
            if (Singleton<GlobalTimer>.Instance.MeasureWideFps
                && !Singleton<GlobalTimer>.Instance.IsWideFPSCoolDown()
                && Singleton<GlobalTimer>.Instance.IsWideFPSLow())
            {
                SingletonMonoBehaviour<RenderTextureManager>.Instance.RequestReslolutionDown();
            }
            _ = SourceMember["UpdateAccount"];
            _ = SourceMember["UpdateController"];
            if (GameFramework.Instance == null)
                Singleton<EffectGenerator>.Instance.UpdateFrame();
            if (!FontManager.REQUEST_CHARACTERS_LATE)
                Singleton<FontManager>.Instance.RequestCharactersInTexture();
        }

        public override GameTuningParam GetGameTuningParam()
        {
            if (_tuning == null)
            {
                _tuning = Source.GetGameTuningParam();

                // Speed up the game and change some scores.
                _tuning._speedBaseRate *= 2f;
                _tuning._ghostIjikeSpeed *= 32f;
                _tuning._ghostMedamaSpeed *= 2f;
                _tuning._ghostSpeed *= 3f;
                _tuning._ghostWaapSpeed *= 2f;
                //_tuning._ScoreGhostLeader1 = 10000;
                //_tuning._ScoreGhostLeader2 = 20000;
                //_tuning._ScoreGhostLeader3 = 40000;
                //_tuning._ScoreGhostLeader4 = 80000;
                //_tuning._ScoreGhostAdd = 0;
                //_tuning._ScoreGhostStart = 0;
                _tuning._ScoreGhostMax = Int32.MaxValue;
                //_tuning._ScoreFruitAdd = 10000;
                //_tuning._ScoreFruitStart = 100000;
                //_tuning._ScoreCookieAddCount = 1;
                //_tuning._ScoreCookieAdd = 4;
                //_tuning._ScoreCookieStart = 2;
                _tuning._ScoreCookieMax = 2;

                // Go through each course.
                foreach (GameTuningParamCource tuningCource in _tuning.GetTuningParamCourceTable())
                {
                    // Remove any course lock restrictions by setting the required rank to rank E.
                    tuningCource.unlockTgtCourceRank1 = 5;
                    tuningCource.unlockTgtCourceRank2 = 5;

                    // Allow infinite minions for any ghost train.
                    GameTuningParamGhost tuningGhost = _tuning.GetTuningParamGhostByCourceId(tuningCource.courceId);
                    tuningGhost._maxTrainGhostCnt = Int32.MaxValue;
                    tuningGhost._maxIjikeTrainGhostCnt = Int32.MaxValue;
                    //tuningGhost._angrySpeedUpMultipleMax = 3.5f;
                }

                UITools.ShowMessage("Courses unlocked and game made insane!", "Faster, longer, stronger....");
            }
            return _tuning;
        }
    }
}

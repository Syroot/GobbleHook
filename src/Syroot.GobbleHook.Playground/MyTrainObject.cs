﻿namespace Syroot.GobbleHook.Playground
{
    public class MyTrainObject : TrainObjectHook
    {
        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        protected MyTrainObject(TrainObject source, int id) : base(source, id) { }

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        public override void AddSpawnMinions(int num)
        {
            // Automatically add 50 spawn minions to any empty train.
            base.AddSpawnMinions(num);
        }
    }
}

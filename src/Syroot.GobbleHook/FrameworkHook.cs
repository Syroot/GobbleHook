﻿using Harmony;
using Syroot.GobbleHook.Core;

namespace Syroot.GobbleHook
{
    /// <summary>
    /// Dispatches <see cref="Framework"/> logic to implementing classes.
    /// </summary>
    public class FrameworkHook : Hook<Framework, FrameworkHook>
    {
        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        /// <summary>
        /// Initializes a new instance of the <see cref="FrameworkHook"/> class for the given <paramref name="source"/>.
        /// </summary>
        /// <param name="source">The hooked instance.</param>
        protected FrameworkHook(Framework source) : base(source) { }

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        /// <summary>
        /// Intercepts <c>private void Awake()</c>.
        /// </summary>
        public virtual void Awake()
        {
            _ = SourceMember[nameof(Awake)];
        }

        /// <summary>
        /// Intercepts <c>public GameTuningParam GetGameTuningParam()</c>.
        /// </summary>
        public virtual GameTuningParam GetGameTuningParam()
        {
            return Source.GetGameTuningParam();
        }

        /// <summary>
        /// Intercepts <c>private void Start()</c>.
        /// </summary>
        public virtual void Start()
        {
            _ = SourceMember[nameof(Start)];
        }

        /// <summary>
        /// Intercepts <c>private void Update()</c>.
        /// </summary>
        public virtual void Update()
        {
            _ = SourceMember[nameof(Update)];
        }

        // ---- CLASSES, STRUCTS & ENUMS -------------------------------------------------------------------------------

        [HarmonyPatch(typeof(Framework))]
        private class CtorPatch
        {
            private static void Postfix(Framework __instance)
            {
                Loader.CreateHook<FrameworkHook>(__instance);
            }
        }

        [HarmonyPatch(typeof(Framework), nameof(Awake))]
        private class AwakePatch : OverridePatch
        {
            private static bool Prefix(Framework __instance)
            {
                return Override(() => Hooks[__instance].Awake());
            }
        }

        [HarmonyPatch(typeof(Framework), "GetGameTuningParam")]
        private class GetGameTuningParamPatch : OverridePatch
        {
            private static bool Prefix(Framework __instance, ref GameTuningParam __result)
            {
                return Override(() => Hooks[__instance].GetGameTuningParam(), ref __result);
            }
        }

        [HarmonyPatch(typeof(Framework), nameof(Start))]
        private class StartPatch : OverridePatch
        {
            private static bool Prefix(Framework __instance)
            {
                return Override(() => Hooks[__instance].Start());
            }
        }

        [HarmonyPatch(typeof(Framework), nameof(Update))]
        private class UpdatePatch : OverridePatch
        {
            private static bool Prefix(Framework __instance)
            {
                return Override(() => Hooks[__instance].Update());
            }
        }
    }
}

﻿using System;
using Harmony;
using PCE2;
using Syroot.GobbleHook.Core;

namespace Syroot.GobbleHook
{
    /// <summary>
    /// Dispatches <see cref="PacmanStateController"/> logic to implementing classes.
    /// </summary>
    public class PacmanStateControllerHook : Hook<PacmanStateController, PacmanStateControllerHook>
    {
        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        /// <summary>
        /// Initializes a new instance of the <see cref="PacmanStateControllerHook"/> class for the given
        /// <paramref name="source"/>.
        /// </summary>
        /// <param name="source">The hooked instance.</param>
        /// <param name="initState">The initial controller state.</param>
        protected PacmanStateControllerHook(PacmanStateController source, Type initState) : base(source) { }

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        /// <summary>
        /// Intercepts <c>private void SetNextState(PacmanState)</c>.
        /// </summary>
        public virtual void SetNextState(PacmanState state)
        {
            Source.SetNextState(state);
        }

        // ---- CLASSES, STRUCTS & ENUMS -------------------------------------------------------------------------------

        [HarmonyPatch(typeof(PacmanStateController), new[] { typeof(Type) })]
        private class CtorPatch
        {
            private static void Postfix(PacmanStateController __instance, Type initState)
            {
                Loader.CreateHook<PacmanStateControllerHook>(__instance, initState);
            }
        }

        [HarmonyPatch(typeof(PacmanStateController), nameof(SetNextState), new[] { typeof(PacmanState) })]
        private class SetNextStatePatch : OverridePatch
        {
            private static bool Prefix(PacmanStateController __instance, PacmanState state)
            {
                return Override(() => Hooks[__instance].SetNextState(state));
            }
        }
    }
}

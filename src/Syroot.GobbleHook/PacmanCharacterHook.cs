﻿using Harmony;
using Syroot.GobbleHook.Core;

namespace Syroot.GobbleHook
{
    /// <summary>
    /// Dispatches <see cref="PacmanCharacter"/> logic to implementing classes.
    /// </summary>
    public class PacmanCharacterHook : Hook<PacmanCharacter, PacmanCharacterHook>
    {
        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        /// <summary>
        /// Initializes a new instance of the <see cref="PacmanStateControllerHook"/> class for the given
        /// <paramref name="source"/>.
        /// </summary>
        /// <param name="source">The hooked instance.</param>
        /// <param name="id">The ID of the game object.</param>
        protected PacmanCharacterHook(PacmanCharacter source, int id) : base(source) { }

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        /// <summary>
        /// Intercepts <c>public void CheckEatEsa()</c>.
        /// </summary>
        public virtual void CheckEatEsa()
        {
            Source.CheckEatEsa();
        }

        // ---- CLASSES, STRUCTS & ENUMS -------------------------------------------------------------------------------

        [HarmonyPatch(typeof(PacmanCharacter), new[] { typeof(int) })]
        private class CtorPatch
        {
            private static void Postfix(PacmanStateController __instance, ref int id)
            {
                Loader.CreateHook<PacmanCharacterHook>(__instance, id);
            }
        }

        [HarmonyPatch(typeof(PacmanCharacter), nameof(CheckEatEsa))]
        private class CheckEatEsaPatch : OverridePatch
        {
            private static bool Prefix(PacmanCharacter __instance)
            {
                return Override(() => Hooks[__instance].CheckEatEsa());
            }
        }
    }
}

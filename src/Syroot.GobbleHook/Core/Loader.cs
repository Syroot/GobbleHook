﻿using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using Harmony;

namespace Syroot.GobbleHook.Core
{
    /// <summary>
    /// Represents the Harmony loader, patching methods to forward them to their respective hooks implemented in
    /// internal and external assemblies.
    /// </summary>
    public static class Loader
    {
        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        /// <summary>
        /// Patches methods in the original game to redirect them to their respective hooks.
        /// Called by the modified game assembly in the static constructor of <see cref="Framework"/>.
        /// </summary>
        public static void Initialize()
        {
            FileLog.Reset();
            Log("Hook invoked.");
            try
            {
                // Instantiate harmony and wire up game logic to the hooks.
                HarmonyInstance harmony = HarmonyInstance.Create("com.syroot.gobblehook");
                harmony.PatchAll(Assembly.GetExecutingAssembly());
                Log("Patching sucessful.");
            }
            catch (Exception ex)
            {
                Log($"Patching failed: {ex}");
            }
        }

        /// <summary>
        /// Writes the given instance to a log file on the desktop if the DEBUG constant is defined.
        /// </summary>
        /// <param name="instance">The instance to log.</param>
        [Conditional("DEBUG")]
        public static void Log(object instance)
        {
            FileLog.Log(instance?.ToString() ?? "null");
        }

        // ---- METHODS (INTERNAL) -------------------------------------------------------------------------------------

        internal static T CreateHook<T>(params object[] parameters)
        {
            Log($"Creating {typeof(T).Name}...");
            Type hookType = GetHookType(typeof(T));
            Log($"\thooking to {hookType}");

            try
            {
                // Invoke a matching constructor.
                MemberAccessor hookMember = new MemberAccessor(hookType);
                return (T)hookMember[null, parameters];
            }
            catch (Exception ex)
            {
                Log($"\thooking failed: {ex}");
                return default(T);
            }
        }

        // ---- METHODS (PRIVATE) --------------------------------------------------------------------------------------

        private static Type GetHookType(Type type)
        {
            // Go through each assembly and try to find an implementation of the given type.
            foreach (string assemblyFile in Directory.GetFiles(@".\PCE2_Data\Managed", "*.dll"))
            {
                Assembly assembly = Assembly.LoadFrom(assemblyFile);
                foreach (Type assemblyType in assembly.GetTypes())
                {
                    if (assemblyType.FullName != type.FullName && type.IsAssignableFrom(assemblyType))
                        return assemblyType;
                }
            }
            return type;
        }
    }
}

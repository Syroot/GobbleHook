﻿using System;
using System.Reflection;

namespace Syroot.GobbleHook.Core
{
    /// <summary>
    /// Provides access to private or internal members of a class. Currently supports fields, constructors, properties,
    /// and methods.
    /// </summary>
    public class MemberAccessor
    {
        // ---- CONSTANTS ----------------------------------------------------------------------------------------------

        private const BindingFlags _defaultBindingFlags = BindingFlags.Instance | BindingFlags.Static
            | BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.FlattenHierarchy;

        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private Type _type;
        private object _instance;

        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        /// <summary>
        /// Initializes a new instance of the <see cref="MemberAccessor"/> class for static member access.
        /// </summary>
        public MemberAccessor(Type type)
        {
            _type = type;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MemberAccessor"/> class for the given
        /// <paramref name="instance"/>.
        /// </summary>
        /// <param name="instance">The object whose private or internal members are made accessible.</param>
        public MemberAccessor(object instance)
            : this(instance.GetType())
        {
            _instance = instance;
        }

        // ---- OPERATORS ----------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets or sets the member with the given <paramref name="memberName"/> and <paramref name="parameters"/>.
        /// For fields and properties, <paramref name="parameters"/> must be <c>null</c>.
        /// For constructors, <paramref name="memberName"/> must be <c>null</c> or <c>.ctor</c>.
        /// The set accessor cannot be used for constructors and methods.
        /// </summary>
        /// <param name="memberName">The name of the member to get or set, or <c>null</c> to retrieve a constructor.
        /// </param>
        /// <param name="parameters">The list of parameters to pass to a constructor or method.</param>
        /// <returns>The retrieved field / property value or constructor / method result.</returns>
        /// <exception cref="MissingMemberException">The requested member was not found.</exception>
        public object this[string memberName = null, params object[] parameters]
        {
            get
            {
                Type[] parameterTypes = GetTypes(parameters);
                if (memberName == null)
                {
                    // Search constructors.
                    foreach (ConstructorInfo constructor in _type.GetConstructors(_defaultBindingFlags))
                    {
                        if (MatchParameters(constructor.GetParameters(), parameterTypes))
                            return constructor.Invoke(parameters);
                    }
                }
                else
                {
                    if (parameters == null || parameters.Length == 0)
                    {
                        // Search fields.
                        foreach (FieldInfo field in _type.GetFields(_defaultBindingFlags))
                        {
                            if (field.Name == memberName)
                                return field.GetValue(_instance);
                        }
                        // Search properties.
                        foreach (PropertyInfo property in _type.GetProperties(_defaultBindingFlags))
                        {
                            if (property.Name == memberName)
                                return property.GetValue(_instance, null);
                        }
                    }
                    // Search methods.
                    foreach (MethodInfo method in _type.GetMethods(_defaultBindingFlags))
                    {
                        if (method.Name == memberName && MatchParameters(method.GetParameters(), parameterTypes))
                            return method.Invoke(_instance, parameters);
                    }
                }
                throw new MissingMemberException(_type.Name, memberName);
            }
            set
            {
                Type[] parameterTypes = GetTypes(parameters);
                if (parameters == null || parameters.Length == 0)
                {
                    // Search fields.
                    foreach (FieldInfo field in _type.GetFields(_defaultBindingFlags))
                    {
                        if (field.Name == memberName)
                        {
                            field.SetValue(_instance, value);
                            return;
                        }
                    }
                    // Search properties.
                    foreach (PropertyInfo property in _type.GetProperties(_defaultBindingFlags))
                    {
                        if (property.Name == memberName)
                        {
                            property.SetValue(_instance, value, null);
                            return;
                        }
                    }
                }
                throw new MissingMemberException(_type.Name, memberName);
            }
        }

        // ---- METHODS (PRIVATE) --------------------------------------------------------------------------------------

        private static Type[] GetTypes(object[] parameters)
        {
            Type[] types = new Type[parameters.Length];
            for (int i = 0; i < parameters.Length; i++)
                types[i] = parameters[i].GetType();
            return types;
        }

        private static bool MatchParameters(ParameterInfo[] parameterInfos, Type[] types)
        {
            if (parameterInfos.Length != types.Length)
                return false;
            for (int i = 0; i < parameterInfos.Length; i++)
            {
                ParameterInfo parameterInfo = parameterInfos[i];
                Type type = types[i];

                //Loader.Log($"\tComparing {parameterInfo.ParameterType} to {type}");
                if (parameterInfos[i].ParameterType.FullName != RemapType(types[i].FullName))
                    return false;
            }
            return true;
        }

        private static string RemapType(string fullName)
        {
            // Combat weird Mono decisions.
            switch (fullName)
            {
                case "System.MonoType":
                    return "System.Type";
                default:
                    return fullName;
            }
        }
    }
}

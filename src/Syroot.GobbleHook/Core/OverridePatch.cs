﻿using System;
using Syroot.GobbleHook.UI;

namespace Syroot.GobbleHook.Core
{
    /// <summary>
    /// Represents the base class for a patch which virtualizes an overridable class method.
    /// </summary>
    internal class OverridePatch
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private static bool _disabled;
        private static bool _executing;

        // ---- METHODS (PROTECTED) ------------------------------------------------------------------------------------

        /// <summary>
        /// Wraps the given <paramref name="action"/> in a way its invocation can be controlled in an override.
        /// </summary>
        /// <param name="action">The code to execute when the overridden method calls the base implementation.</param>
        /// <returns><c>false</c> to not run the original code afterwards.</returns>
        protected static bool Override(Action action)
        {
            // Always run the original method if the override is disabled or calling the base implementation.
            if (_disabled || _executing)
                return true;

            _executing = true;
            try
            {
                action();
                return false;
            }
            catch (Exception ex)
            {
                Loader.Log(ex);
                UITools.ShowMessage("GobbleHook Error", ex);
                _disabled = true;
                return true;
            }
            finally
            {
                _executing = false;
            }
        }

        /// <summary>
        /// Wraps the given <paramref name="func"/> in a way its invocation can be controlled in an override.
        /// </summary>
        /// <param name="func">The code to execute when the overridden method calls the base implementation.</param>
        /// <param name="__result">The variable to return the result of the function in.</param>
        /// <returns><c>false</c> to not run the original code afterwards.</returns>
        protected static bool Override<TResult>(Func<TResult> func, ref TResult __result)
        {
            // Always run the original method if the override is disabled or calling the base implementation.
            if (_disabled || _executing)
                return true;

            _executing = true;
            try
            {
                __result = func();
                return false;
            }
            catch (Exception ex)
            {
                Loader.Log(ex);
                UITools.ShowMessage("GobbleHook Error", ex);
                _disabled = true;
                return true;
            }
            finally
            {
                _executing = false;
            }
        }
    }
}

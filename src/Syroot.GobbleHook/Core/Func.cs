﻿namespace Syroot.GobbleHook.Core
{
    // Required due to Mono not shipping with it.

    /// <summary>
    /// Represents a method which returns an instance of type <typeparamref name="TResult"/> and has no parameters.
    /// </summary>
    /// <typeparam name="TResult">The type of the returned instance.</typeparam>
    /// <returns>The returned instance of type <typeparamref name="TResult"/>.</returns>
    public delegate TResult Func<TResult>();
}

﻿using System.Collections.Generic;

namespace Syroot.GobbleHook.Core
{
    /// <summary>
    /// Represents a dispatcher redirecting messages of an instance of type <typeparamref name="TSource"/> to an
    /// instance of <typeparamref name="TTarget"/>.
    /// </summary>
    /// <typeparam name="TSource">The type of the hooked instance.</typeparam>
    /// <typeparam name="TTarget">The type of the hook.</typeparam>
    public abstract class Hook<TSource, TTarget>
        where TTarget : Hook<TSource, TTarget>
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        internal static readonly IDictionary<TSource, TTarget> Hooks = new Dictionary<TSource, TTarget>();

        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        /// <summary>
        /// Initializes a new instance of the <see cref="Hook{TSource, TTarget}"/> class for the given
        /// <paramref name="source"/>.
        /// </summary>
        /// <param name="source">The hooked instance.</param>
        protected Hook(TSource source)
        {
            Hooks.Add(source, (TTarget)this);

            Source = source;
            SourceMember = new MemberAccessor(source);
        }

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets the wrapped instance.
        /// </summary>
        public TSource Source { get; }

        /// <summary>
        /// Gets an <see cref="MemberAccessor"/> providing access to all members of <see cref="Source"/>, including
        /// private or internal ones.
        /// </summary>
        public MemberAccessor SourceMember { get; }
    }
}

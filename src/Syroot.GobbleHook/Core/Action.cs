﻿namespace Syroot.GobbleHook.Core
{
    // Required due to Mono not shipping with it.

    /// <summary>
    /// Represents a method which has no parameters.
    /// </summary>
    public delegate void Action();
}

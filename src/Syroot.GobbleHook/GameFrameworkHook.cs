﻿using Harmony;
using Syroot.GobbleHook.Core;

namespace Syroot.GobbleHook
{
    /// <summary>
    /// Dispatches <see cref="GameFramework"/> logic to implementing classes.
    /// </summary>
    public class GameFrameworkHook : Hook<GameFramework, GameFrameworkHook>
    {
        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        /// <summary>
        /// Initializes a new instance of the <see cref="GameFrameworkHook"/> class for the given
        /// <paramref name="source"/>.
        /// </summary>
        /// <param name="source">The hooked instance.</param>
        /// <param name="mode">The <see cref="ModeGame"/> to instantiate the framework for.</param>
        protected GameFrameworkHook(GameFramework source, ModeGame mode) : base(source) { }

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        /// <summary>
        /// Intercepts <c>private void OnReadyGo()</c>.
        /// </summary>
        public virtual void OnReadyGo()
        {
            _ = SourceMember[nameof(OnReadyGo)];
        }

        /// <summary>
        /// Intercepts <c>private void OnMoveMapComplete()</c>.
        /// </summary>
        public virtual void OnMoveMapComplete()
        {
            _ = SourceMember[nameof(OnMoveMapComplete)];
        }

        // ---- CLASSES, STRUCTS & ENUMS -------------------------------------------------------------------------------

        [HarmonyPatch(typeof(GameFramework), new[] { typeof(ModeGame) })]
        private class CtorPatch
        {
            private static void Postfix(GameFramework __instance, ModeGame mode)
            {
                Loader.CreateHook<GameFrameworkHook>(__instance, mode);
            }
        }

        [HarmonyPatch(typeof(GameFramework), "OnReadyGo")]
        private class OnReadyGoPatch : OverridePatch
        {
            private static bool Prefix(GameFramework __instance)
            {
                return Override(() => Hooks[__instance].OnReadyGo());
            }
        }

        [HarmonyPatch(typeof(GameFramework), "OnMoveMapComplete")]
        private class OnMoveMapCompletePatch : OverridePatch
        {
            private static bool Prefix(GameFramework __instance)
            {
                return Override(() => Hooks[__instance].OnMoveMapComplete());
            }
        }
    }
}

﻿using System.Collections;
using Harmony;
using Syroot.GobbleHook.Core;
using UI;

namespace Syroot.GobbleHook.UI
{
    /// <summary>
    /// Dispatches <see cref="ModeLogo"/> logic to implementing classes.
    /// </summary>
    internal class ModeLogoHook : Hook<ModeLogo, ModeLogoHook>
    {
        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        /// <summary>
        /// Initializes a new instance of the <see cref="ModeLogo"/> class for the given <paramref name="source"/>.
        /// </summary>
        /// <param name="source">The hooked instance.</param>
        protected ModeLogoHook(ModeLogo source) : base(source) { }

        // ---- METHODS (INTERNAL) -------------------------------------------------------------------------------------

        /// <summary>
        /// Intercepts <c>protected override IEnumerator _BeginScene()</c>.
        /// </summary>
        internal virtual IEnumerator _BeginScene()
        {
            // TODO: Support yielding IEnumerator items somehow before making this public.
            // End the logo scene for now and display hook message with sound.
            UITools.ShowMessage("GobbleHook loaded!", "Custom game logic is now available.");
            SingletonMonoBehaviour<SoundManager>.Instance.Play(BGMTagEnum.BGMTag.StartShort);
            Source.End(false);
            return null;
        }

        // ---- CLASSES, STRUCTS & ENUMS -------------------------------------------------------------------------------

        [HarmonyPatch(typeof(ModeLogo))]
        private class CtorPatch
        {
            private static void Postfix(ModeLogo __instance)
            {
                Loader.CreateHook<ModeLogoHook>(__instance);
            }
        }

        [HarmonyPatch(typeof(ModeLogo), nameof(_BeginScene))]
        private class _BeginScenePatch : OverridePatch
        {
            private static bool Prefix(ModeLogo __instance)
            {
                return Override(() => Hooks[__instance]._BeginScene());
            }
        }
    }
}

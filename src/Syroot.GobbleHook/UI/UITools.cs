﻿using System;
using UI;

namespace Syroot.GobbleHook.UI
{
    /// <summary>
    /// Represents a collection of helper methods to modify and access the game.
    /// </summary>
    public static class UITools
    {
        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        /// <summary>
        /// Displays an asynchronous OK message box displaying information on the given instance.
        /// </summary>
        /// <param name="instance">The instance to display.</param>
        public static void ShowMessage(object instance) => ShowMessage(null, instance);

        /// <summary>
        /// Displays an asynchronous OK message box displaying information on the given exception.
        /// </summary>
        /// <param name="ex">The <see cref="Exception"/> to display.</param>
        public static void ShowMessage(Exception ex) => ShowMessage(ex.GetType(), ex);

        /// <summary>
        /// Displays an asynchronous OK message box with the given <paramref name="title"/> and
        /// <paramref name="message"/>.
        /// </summary>
        /// <param name="title">The text to display at the top of the message box.</param>
        /// <param name="message">The smaller text to display at the bottom of the message box.</param>
        public static void ShowMessage(object title, object message)
        {
            PCE2DialogParam dialogParam = new PCE2DialogParam();
            dialogParam.LayoutMessage(title?.ToString(), message?.ToString());
            SingletonMonoBehaviour<DialogManager>.Instance.ShowDialog(dialogParam);
        }
    }
}

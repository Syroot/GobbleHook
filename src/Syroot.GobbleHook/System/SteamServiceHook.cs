﻿using Harmony;
using PCE2System;
using Steamworks;

namespace Syroot.GobbleHook.System
{
    internal sealed class SteamServiceHook
    {
        // ---- CLASSES, STRUCTS & ENUMS -------------------------------------------------------------------------------

        [HarmonyPatch(typeof(SteamService), "UploadScore")]
        private class UploadScorePatch
        {
            private static bool Prefix(ref ulong score,
                CallResult<LeaderboardScoreUploaded_t>.APIDispatchDelegate dispatch, ref bool __result)
            {
                dispatch(new LeaderboardScoreUploaded_t(), true);
                __result = true;
                return false;
            }
        }
    }
}

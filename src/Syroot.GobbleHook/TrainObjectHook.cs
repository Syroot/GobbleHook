﻿using Harmony;
using Syroot.GobbleHook.Core;

namespace Syroot.GobbleHook
{
    /// <summary>
    /// Dispatches <see cref="TrainObject"/> logic to implementing classes.
    /// </summary>
    public class TrainObjectHook : Hook<TrainObject, TrainObjectHook>
    {
        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        /// <summary>
        /// Initializes a new instance of the <see cref="TrainObject"/> class for the given <paramref name="source"/>.
        /// </summary>
        /// <param name="source">The hooked instance.</param>
        /// <param name="id">The ID of the game object.</param>
        protected TrainObjectHook(TrainObject source, int id) : base(source) { }

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        /// <summary>
        /// Intercepts <c>public void AddSpawnMinions(int num)</c>.
        /// </summary>
        public virtual void AddSpawnMinions(int num) => Source.AddSpawnMinions(num);

        /// <summary>
        /// Intercepts <c>public int GetTrainGhostMaxNum()</c>.
        /// </summary>
        public virtual int GetTrainGhostMaxNum() => Source.GetTrainGhostMaxNum();

        // ---- CLASSES, STRUCTS & ENUMS -------------------------------------------------------------------------------

        [HarmonyPatch(typeof(TrainObject), new[] { typeof(int) })]
        private class CtorPatch
        {
            private static void Postfix(TrainObject __instance, ref int id)
            {
                Loader.CreateHook<TrainObjectHook>(__instance, id);
            }
        }

        [HarmonyPatch(typeof(TrainObject), nameof(AddSpawnMinions))]
        private class AddSpawnMinionsPatch : OverridePatch
        {
            private static bool Prefix(TrainObject __instance, ref int num)
            {
                int numValue = num;
                return Override(() => Hooks[__instance].AddSpawnMinions(numValue));
            }
        }

        [HarmonyPatch(typeof(TrainObject), nameof(GetTrainGhostMaxNum))]
        private class GetTrainGhostMaxNumPatch : OverridePatch
        {
            private static bool Prefix(TrainObject __instance, ref int __result)
            {
                return Override(() => Hooks[__instance].GetTrainGhostMaxNum(), ref __result);
            }
        }
    }
}
